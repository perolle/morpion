export const PLAYER1 = Symbol('player 1') ;
export const PLAYER2 = Symbol('player 2') ;
/**
*
Liste des alignements de coups gagnants possibles.
*/
const WIN_PATTERNS = [
    [0, 1, 2], // Ligne 0
    [3, 4, 5], // Ligne 1
    [6, 7, 8], // Ligne 2
    [0, 3, 6], // Colonne 0
    [1, 4, 7], // Colonne 1
    [2, 5, 8], // Colonne 2
    [0, 4, 8], // Diagonale ↘
    [2, 4, 6] // Diagonale ↙
    ] ;

  
/**
* Représente une partie de Morpion.
*/
export class Game {
/**
* Crée une nouvelle partie.
*/
    constructor() {
        //crée un tableau de 9 éléments à la valeur null
        this.board = Array(9).fill(null) ;
        this.currentPlayer = PLAYER1 ;
        this.movesCount = 0 ;
        
    }

/**
* Tente de jouer le coup indiqué pour le joueur courant.
*
* Retourn `true` si le coup a pu être joué. Sinon, retourne `false`.
*
* @param cell Le numéro de la case où jouer le coup.
* @returns {boolean} La réussite ou l’échec du coup.
*/
    move(cell) {
        // Si le joueur est `null` ou si la case n’est pas `null
        // le coup ne peut pas être joué
        if (this.currentPlayer === null || this.board[cell] !== null ) {
        // On retourne `false` et on ne fait rien d'autre.
            return false ;
        }
        //ajoute la case au compteur
        this.movesCount++ ;
        //ajoute le symbole du joueur dans la case vide
        this.board[cell] = this.currentPlayer ;
        return true ;
    }
    /**
    * Vérifie si le joueur courant a réaliser une condition de victoire.
    *
    * En cas de victoire, retourne la liste des cases gagnantes.
    * Sinon retourne `false`.
    *
    * @returns {boolean|array}
    Coup gagnant, sinon `false`.
    */
    checkWin() {
        //crée une variable objet de la liste des coups gagnant
        //pour chaque (crée une variable de la liste des combinaisons gagnantes)
        for (let pattern of WIN_PATTERNS) {
        //cree une variable de match vraie répertorier dans la liste des combinaisons gagnantes    
        let match = true ;
        //pour chaque(cree une variable de la variable de la liste gagnante)
            for (let cell of pattern) {
                // cet objet vérifie si les case du board ont le même symbole
                // si lôbjet n'as pas les même symbole
                if (this.board[cell] !== this.currentPlayer) {
                    //match retourne faux et pas de gagnant
                    match = false ;
                    //la vérification s'arrête
                    break;
                }
            }
            // si match égale true
            if (match) {
            //affiche la combinaison gagnante    
            return pattern;
            }
        }
    //retourne faux si le coup jouer n'amène pas la victoire
        return false ;
    }
    
    /**
    * Marque la partie comme terminée.
    */
    terminate() {
        this.currentPlayer = null ;
    }
    /**
     * Vérifie si la partie a été marquée comme terminée.
     */
    isTerminated() {
        return this.currentPlayer === null ;
    }
    /**
    * Change le joueur courant.
    *
    * Marque la fin de partie si le nombre de coups maximum est atteint.
    */
    switchPlayer() {
        if (this.movesCount === 9) {
            this.terminate() ;
            alert("Partie nulle. Voulez-vous rejouer?" + "\n" +  "cliquez sur nouvelle manche");
            return ;
           
        }
        switch (this.currentPlayer) {
            case PLAYER1 :
            this.currentPlayer = PLAYER2 ;
        break ;
            case PLAYER2 :
            this.currentPlayer = PLAYER1 ;
        break ;
            case null :
        break ;
        }
    }
}

