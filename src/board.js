import {Game, PLAYER1,} from './game' ;
/**
* Interface graphique pour le jeu de morpion
*/
export class Board {
    /**
    * Affiche un plateau de jeu interactif dans l’élément spécifié.
    *
    * @param root
    Élément racine
    */
    constructor(root) {
        this.root = root ;

        const board = document.createElement('div') ;
            board.classList.add('board') ;
            this.root.appendChild(board) ;

        // On crée un `div` qui représente la grille de jeu et qui contiendra les cases
        const grid = document.createElement('div') ;
            grid.classList.add('grid') ;
            board.appendChild(grid) ;

        // On crée les cases et les ajoutes à la grille
        this.cells = [] ;
        for (let i = 0 ; i < 9 ; i++) {
            const cell = document.createElement('div') ;
            cell.classList.add('cell') ;

            const symbol = document.createElement('div') ;
            symbol.classList.add('symbol') ;
            cell.appendChild(symbol) ;

            this.cells.push(cell) ;
            grid.appendChild(cell) ;
        }
        //récupère le nom des gamer pour afficher le vainqueur
        this.nomJoueur();
        
        this.morpion = new Game();
        //refresh la grille
        document.querySelector("#newSet").onclick=() => {
            this.morpion = new Game();
            for (let clean of this.cells){
                clean.classList.remove('player1');
                clean.classList.remove('player2');
                clean.classList.remove('win');
        
            }
        }//fin onclick newSet
        
        for(let index of this.cells.keys()){
            this.cells[index].onclick =()=> this.onClickCell(index);
        }
        /*correction vincent pour l'ajout des nom et du reset
        // On ajoute 2 inputs pour les noms des joueurs
        this.nameInput1 = document.createElement('input');
        this.nameInput1.type = 'text';
        this.nameInput1.placeholder = 'Joueur 1';
        this.root.appendChild(this.nameInput1);

        this.nameInput2 = document.createElement('input');
        this.nameInput2.type = 'text';
        this.nameInput2.placeholder = 'Joueur 2';
        this.root.appendChild(this.nameInput2);

        // On ajoute un bouton "reset"
        const button = document.createElement('button');
        button.classList.add('reset');
        button.append('Reset');
        button.onclick = () => this.reset();
        this.root.appendChild(button)
        }*/

        this.scoreP1 = 0;
        this.scoreP2 = 0;
    }
//prend les données du prompt et les affiches si prompt vide affiche player1/2    
nomJoueur(){
    this.nom = [];
    let namePlayer1 = this.nom['player 1'] = window.prompt("player1: Quel est votre nom?");
        if(namePlayer1 == undefined || namePlayer1==""){ 
                document.getElementById("namePlayer1").innerHTML = "player 1";
            }else{document.getElementById("namePlayer1").innerHTML = namePlayer1;
            }
            //si player 1 n'est pas vide
            if(namePlayer1!=undefined){
                let namePlayer2 = this.nom ["player 2"] = window.prompt("player2: Quel est votre nom?");
                    if(namePlayer2 == undefined || namePlayer2==""){
                        document.getElementById("namePlayer2").innerHTML = "player 2";
                    }else{ document.getElementById("namePlayer2").innerHTML = namePlayer2;
                    }
            }
}


/**
* Gère les cliques des joueurs sur les cases.
*
* @param index
Indice de la case cliquée
*/
onClickCell(index) {
    const cell = this.cells[index] ;
    // Si le coup n’est pas possible, on quitte la fonction sans rien faire de plus.
    if ( !this.morpion.move(index)) {
        return;
       }

    // On indique que le joueur courant à jouer un coup dans la case cliquée
    // en ajoutant la classe CSS adéquate.
    if (this.morpion.currentPlayer === PLAYER1) {
        cell.classList.add('player1');
    } else {
        cell.classList.add('player2');
    }
    let winPattern = this.morpion.checkWin() ;
    if ( !winPattern) {
        // On indique que c’est l’autre joueur qui joue à présent.
        this.morpion.switchPlayer() ;
    } else {
        let currentPlayer = this.morpion.currentPlayer;
        let playerNom = this.nom[currentPlayer.description];
        if(playerNom === null){
            playerNom = currentPlayer.description;
        };
        this.morpion.terminate() ;
        this.highlight(winPattern) ;
        //associe le nom du gagnant et son score gagnant
        if(currentPlayer.description === "player 1"){
            document.getElementById("score1").innerHTML = ++this.scoreP1;
        }else{
             document.getElementById("score2").innerHTML = ++this.scoreP2;
        } 
        //popup alert nom du gagnant     
        let rejouer = window.alert(playerNom +" a gagné. Voulez-vous rejouer?" + "\n" + "cliquez sur nouvelle manche");
        if(rejouer){
            return;
        }
    }
}
    /**
    * Met en évidence les cases indiquées.
    *
    * @param indexes
    Liste des cases à mettre en évidence
    */
    highlight(indexes) {
        for (let index of indexes) {
            this.cells[index].classList.add('win');
        }

    }//fin highlight

    
    

}//fin classe board

